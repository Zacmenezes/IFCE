package agents;

import problems.AbstractProblem;
import problems.Problem;
import problems.RomaniaMapProblem;
import strategies.AStar;
import strategies.AbstractTreeSearch;
import strategies.BCU;
import strategies.BFS;
import strategies.BidirectionalSearch;
import strategies.DFS;
import strategies.DFSVisited;
import strategies.DLS;
import strategies.DLSIterative;
import strategies.GreedySearch;
import strategies.Strategy;
import util.OptionsUtil;

public class SimpleAgent {

	AbstractTreeSearch treeSearchStrategy;

	public SimpleAgent(Strategy strategy, AbstractProblem formulatedProblem, int limit) {
		treeSearchStrategy = chooseStrategy(strategy, formulatedProblem, limit);
	}

	public void resolveProblem() {
		try {
			treeSearchStrategy.treeSearch().forEach(node -> System.out.println(node.getAcao()));
		} catch (StackOverflowError | NullPointerException E) {
			E.printStackTrace();
			System.out.println("Falha!");
		}
	}

	public AbstractTreeSearch chooseStrategy(Strategy strategy, AbstractProblem formulatedProblem, int limit) {

		switch (strategy) {
		case BFS:
			return treeSearchStrategy = new BFS(formulatedProblem);
		case DFS:
			return treeSearchStrategy = new DFS(formulatedProblem);
		case DFSVisited:
			return treeSearchStrategy = new DFSVisited(formulatedProblem);
		case DLS:
			return treeSearchStrategy = new DLS(formulatedProblem, limit);
		case DLSIterative:
			return treeSearchStrategy = new DLSIterative(formulatedProblem, limit);
		case BCU:
			return treeSearchStrategy = new BCU(formulatedProblem);
		case AStar:
			return treeSearchStrategy = new AStar(formulatedProblem);
		case GREEDYSEARCH:
			return treeSearchStrategy = new GreedySearch(formulatedProblem);
		case BB:
			return treeSearchStrategy = new BidirectionalSearch(
					formulatedProblem,
					chooseStrategy(OptionsUtil.getStrategy(OptionsUtil.getRomaniaStrategyOption()),
							new RomaniaMapProblem(formulatedProblem.getInitialState(),
							formulatedProblem.getObjectiveState(), Problem.ROMENIA),
							limit),
					chooseStrategy(OptionsUtil.getStrategy(OptionsUtil.getRomaniaStrategyOption()),
							new RomaniaMapProblem(formulatedProblem.getObjectiveState(),
							formulatedProblem.getInitialState(), Problem.ROMENIA),
							limit));
		default:
			System.out.println("opção inválida");
			return null;
		}
	}

}
