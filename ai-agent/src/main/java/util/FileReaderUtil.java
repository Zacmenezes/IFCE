package util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReaderUtil {
	
	public static List<String> readFile(String nomeArquivo) {
		try {
			URI uri = FileReaderUtil.class.getClassLoader().getResource(nomeArquivo).toURI();
			List<String> lines = Files.readAllLines(Paths.get(uri));
			return lines;
			
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
