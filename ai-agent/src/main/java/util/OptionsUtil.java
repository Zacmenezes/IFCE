package util;

import java.util.Scanner;

import strategies.Strategy;

public class OptionsUtil {

	private static Scanner scanner;

	public static void createScanner() {
		scanner = new Scanner(System.in);
	}

	public static void closeScanner() {
		scanner.close();
	}

	public static String getInitialState() {
		scanner = new Scanner(System.in);
		System.out.println("Digite o estado inicial: ");
		String origin;
		origin = scanner.nextLine();
		return origin;
	}

	public static String getObjectiveState() {
		scanner = new Scanner(System.in);
		System.out.println("Digite o estado objetivo: ");
		String objective = scanner.nextLine();
		return objective;
	}

	public static int getProblemOption() {
		scanner = new Scanner(System.in);
		int result = 0;
		System.out.println("+-----------------------------------+");
		System.out.println("|Escolha um problema:               |");
		System.out.println("+-------------+---------------------+");
		System.out.println("| 0 - Romênia | 1 - Aspirador de pó |");
		System.out.println("+-------------+---------------------+");
		result = scanner.nextInt();
		return result;
	}

	public static int getRomaniaStrategyOption() {
		scanner = new Scanner(System.in);
		int result = 0;
		System.out.println("+-----------------------------------------------------------------+");
		System.out.println("|Escolha uma estratégia:                                          |");
		System.out.println("+---------+---------+----------------+---------+------------------+");
		System.out.println("| 0 - BFS | 1 - DFS | 2 - DFSVisited | 3 - DLS | 4 - DLSIterative |");
		System.out.println("+---------+---------+----------------+---------+------------------+");
		System.out.println("| 5 - BCU | 6 - A*  | 7 - GreedyS.   | 8 - Bidirectional Search   |");
		System.out.println("+---------+---------+----------------+----------------------------+");

		result = scanner.nextInt();
		return result;
	}

	public static int getVaccumCleanerStrategyOption() {
		Scanner scanner = new Scanner(System.in);
		int result = 0;
		System.out.println("+-----------------------------------------------------------------+");
		System.out.println("|Escolha uma estratégia:                                          |");
		System.out.println("+---------+---------+----------------+---------+------------------+");
		System.out.println("| 0 - BFS | 1 - DFS | 2 - DFSVisited | 3 - DLS | 4 - DLSIterative |");
		System.out.println("+---------+---------+----------------+---------+------------------+");

		result = scanner.nextInt();
		scanner.close();
		return result;
	}

	public static int getLimit() {
		scanner = new Scanner(System.in);
		int result = 0;
		System.out.println("digite um limite: ");
		result = scanner.nextInt();
		return result;
	}

	public static Strategy getStrategy(int option) {
		switch (option) {
		case 0:
			return Strategy.BFS;
		case 1:
			return Strategy.DFS;
		case 2:
			return Strategy.DFSVisited;
		case 3:
			return Strategy.DLS;
		case 4:
			return Strategy.DLSIterative;
		case 5:
			return Strategy.BCU;
		case 6:
			return Strategy.AStar;
		case 7:
			return Strategy.GREEDYSEARCH;
		case 8:
			return Strategy.BB;
		default:
			return null;
		}

	}
}
