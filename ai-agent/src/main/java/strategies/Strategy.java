package strategies;

public enum Strategy {
	BFS, DFS, DFSVisited, DLS, DLSIterative, AStar, BCU, GREEDYSEARCH, BB;
}
