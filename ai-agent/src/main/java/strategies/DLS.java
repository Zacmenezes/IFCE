package strategies;

import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class DLS extends DFS {

	int limit;

	public DLS(AbstractProblem formulatedProblem, int limit) {
		super(formulatedProblem);
		this.limit = limit;
	}

	@Override
	public List<Node> treeSearch() {
		initFringe();
		while (!fringe.isEmpty()) {
			Node currentNode = getFirstNode();
			if (formulatedProblem.goalTest(currentNode))
				return solution(currentNode);
			else if (currentNode.getProfundidade() != limit)
				addToFringe(currentNode);
		}
		return null;
	}

}
