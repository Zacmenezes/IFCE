package strategies;

import java.util.LinkedList;
import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class GreedySearch extends BCU {

	public GreedySearch(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}

	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new LinkedList<>();
		sucessorFn(node).forEach((estado, custo) -> {
			sucessors.add(nodeFactory.createNode(node, estado,
					Math.round(formulatedProblem.getHeuristic(estado, formulatedProblem.getObjectiveState()))));
		});
		return sucessors;
	}

}
