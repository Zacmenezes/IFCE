package strategies;

import java.util.LinkedList;
import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class AStar extends BCU {

	public AStar(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}
	
	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new LinkedList<>();
		sucessorFn(node).forEach((estado, custo) -> {
			sucessors.add(nodeFactory.createNode(node, estado, calculateCost(node, estado, custo)));
		});
		return sucessors;
	}
	
	private Integer calculateCost(Node pai, String estado, Integer custo) {
		Integer parentCost = pai.getCusto();
		Float parentHeuristic = (parentCost == 0)? 0:formulatedProblem.getHeuristic(pai.getEstado(),formulatedProblem.getObjectiveState());
		Float currentStateHeuristic = formulatedProblem.getHeuristic(estado, formulatedProblem.getObjectiveState());
		Integer finalCost = parentCost - Math.round(parentHeuristic) + custo + Math.round(currentStateHeuristic);
		return finalCost;
	}

}
