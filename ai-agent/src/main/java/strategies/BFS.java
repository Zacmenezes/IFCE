package strategies;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import problems.AbstractProblem;
import structures.Node;

public class BFS extends AbstractTreeSearch {

	public BFS(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}

	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new LinkedList<>();
		sucessorFn(node).forEach((estado, custo) -> {
			sucessors.add(nodeFactory.createNode(node, estado, custo));
		});
		return sucessors;
	}

	@Override
	public void initFringe() {
		fringe = new LinkedList<>();
		fringe.add(formulatedProblem.getInitialNode());
	}

	@Override
	public void addToFringe(Node node) {
		fringe.addAll(expand(node));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Node getFirstNode() {
		return ((Queue<Node>) fringe).poll();
	}

}
