package strategies;

import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class DLSIterative extends DLS {

	public DLSIterative(AbstractProblem formulatedProblem, int limit) {
		super(formulatedProblem, limit);
	}
	
	@Override
	public List<Node> treeSearch() {
		List<Node> result = null;
		
		while(limit>=0) {
			System.out.println("nível: "+limit);
			result = super.treeSearch();
			if(result!=null)
				break;
			System.out.println("corte");
			limit++;
		}
		return result;
	}

}
