package strategies;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import problems.AbstractProblem;
import structures.Node;

public class DFS extends AbstractTreeSearch {

	public DFS(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}

	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new ArrayList<>();
		sucessorFn(node).forEach((estado, custo) -> sucessors.add(nodeFactory.createNode(node, estado, custo)));
		return sucessors;
	}

	@Override
	public void initFringe() {
		fringe = new Stack<>();
		fringe.add(formulatedProblem.getInitialNode());
	}

	@Override
	public void addToFringe(Node node) {
		fringe.addAll(expand(node));
	}

	@Override
	public Node getFirstNode() {
		return ((Stack<Node>) fringe).pop();
	}

}
