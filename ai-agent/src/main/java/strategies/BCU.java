package strategies;

import java.util.LinkedList;
import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class BCU extends BFS {

	public BCU(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}
	
	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new LinkedList<>();
		sucessorFn(node).forEach((estado, custo) -> {
			sucessors.add(nodeFactory.createNode(node, estado, calculateCost(node, custo)));
		});
		return sucessors;
	}
	
	@Override
	public void addToFringe(Node node) {
		fringe.addAll(expand(node));
		fringe.sort((Node one, Node two) -> one.getCusto() - two.getCusto());
	}
	
	private Integer calculateCost(Node pai, Integer custo) {
		Integer custoPai = pai.getCusto();
		Integer custoFinal = custoPai + custo;
		return custoFinal;
	}

}
