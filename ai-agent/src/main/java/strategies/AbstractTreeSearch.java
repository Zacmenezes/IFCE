package strategies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import problems.AbstractProblem;
import structures.Node;
import structures.NodeFactory;

public abstract class AbstractTreeSearch  {

	protected AbstractProblem formulatedProblem;
	protected NodeFactory nodeFactory;
	protected List<Node> fringe;
	
	public AbstractTreeSearch(AbstractProblem formulatedProblem) {
		this.formulatedProblem = formulatedProblem;
		this.nodeFactory = new NodeFactory(formulatedProblem);
	}
	
	public List<Node> treeSearch() {
		initFringe();
		while (!fringe.isEmpty()) {
			Node currentNode = getFirstNode();
			if (formulatedProblem.goalTest(currentNode))
				return solution(currentNode);
			else
				addToFringe(currentNode);
		}
		return null;
	}
	
	public abstract List<Node> expand(Node node);
	
	public Map<String, Integer> sucessorFn(Node node) {
		return formulatedProblem.getSucessors(node.getEstado());
	}
	
	public List<Node> solution(Node node) {
		List<Node> solution = new ArrayList<>();
		if(node == null)
			return solution;
		Node currentNode = node;
		
		while (currentNode.getPai() != null) {
			solution.add(currentNode);
			currentNode = currentNode.getPai();
		}
		Collections.reverse(solution);
		return solution;
	}
	
	public abstract void initFringe();
	
	public abstract void addToFringe(Node node);
	
	public abstract Node getFirstNode();
}
