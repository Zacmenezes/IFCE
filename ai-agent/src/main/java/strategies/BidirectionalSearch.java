package strategies;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import problems.AbstractProblem;
import structures.Node;

public class BidirectionalSearch extends AbstractTreeSearch {

	private AbstractTreeSearch firstSearch;
	private AbstractTreeSearch secondSearch;

	public BidirectionalSearch(AbstractProblem formulatedProblem, AbstractTreeSearch search1,
			AbstractTreeSearch search2) {
		super(formulatedProblem);
		this.firstSearch = search1;
		this.secondSearch = search2;
		this.fringe = new LinkedList<>();
		firstSearch.initFringe();
		secondSearch.initFringe();
	}

	@Override
	public List<Node> treeSearch() {
		Node firstSearchCurrentNode = null;
		Node secondSearchCurrentNode = null;
		String interssection;
		while (!firstSearch.fringe.isEmpty() && !secondSearch.fringe.isEmpty()) {

			firstSearchCurrentNode = firstSearch.getFirstNode();
			interssection = goalTest(firstSearchCurrentNode, firstSearch.fringe, secondSearch.fringe);

			System.out.println("Nó atual busca1: " + firstSearchCurrentNode.getEstado());

			if (exists(interssection))
				return solution(interssection, firstSearchCurrentNode, secondSearchCurrentNode);
			firstSearch.addToFringe(firstSearchCurrentNode);
			
			System.out.println(" BORDA 1: " + firstSearch.fringe);

			secondSearchCurrentNode = secondSearch.getFirstNode();
			interssection = null;
			interssection = goalTest(secondSearchCurrentNode, secondSearch.fringe, firstSearch.fringe);

			System.out.println("Nó atual busca2: " + secondSearchCurrentNode.getEstado());
			
			if (exists(interssection))
				return solution(interssection, secondSearchCurrentNode, firstSearchCurrentNode);
			secondSearch.addToFringe(secondSearchCurrentNode);

			System.out.println(" BORDA 2: " + secondSearch.fringe);

		}
		return null;
	}

	public String goalTest(Node list1CurrentNode, List<Node> list1, List<Node> list2) {

		for (Node node1 : list1) {
			for (Node node2 : list2) {
				if (node1.getEstado().equals(node2.getEstado())) {
					System.out.println(2);
					return node1.getEstado();
				}
				if (list1CurrentNode.getEstado().equals(node2.getEstado())) {
					System.out.println(1);
					return list1CurrentNode.getEstado();
				}
			}
		}
		return null;
	}

	public List<Node> solution(String interssection, Node firstSearchCurrentNode, Node secondSearchCurrentNode ) {

		System.out.println("interssection: " + interssection);
		
		Node firstSearchNode = getInterssectionNode(interssection, firstSearch.fringe, firstSearchCurrentNode);
		Node secondSearchNode = getInterssectionNode(interssection, secondSearch.fringe, secondSearchCurrentNode);
		
		fringe.addAll(firstSearch.solution(firstSearchNode));
		fringe.addAll(reverseSolution(secondSearchNode, firstSearchNode));
		return fringe;
	}

	private List<Node> reverseSolution(Node secondSearchNode, Node firstSearchNode) {
		List<Node> solution = new ArrayList<>();
		Node currentNode = secondSearchNode;
		Node parentNode = firstSearchNode;

		if (secondSearchNode == null || firstSearchNode == null)
			return solution;

		while (currentNode.getPai() != null) {
			currentNode.setAcao("em(" + parentNode.getEstado() + ") -> ir(" + currentNode.getPai().getEstado() + ")");
			solution.add(currentNode);
			currentNode = currentNode.getPai();
			parentNode = currentNode;
		}

		return solution;
	}
	
	private Node getInterssectionNode(String interssectionState, List<Node> fringe, Node currentNode) {
		for (Node node : fringe) {
			if (interssectionState.equals(node.getEstado())) 
				return node;
			else if (interssectionState.equals(currentNode.getEstado())) 
				return currentNode;
		}
		return null;
	}
	
	private boolean exists(String interssection) {
		return interssection != null;
	}

	@Override
	public List<Node> expand(Node node) {
		return null;
	}

	@Override
	public void initFringe() {
		// TODO Auto-generated method stub
	}

	@Override
	public void addToFringe(Node node) {
		// TODO Auto-generated method stub
	}

	@Override
	public Node getFirstNode() {
		// TODO Auto-generated method stub
		return null;
	}

}
