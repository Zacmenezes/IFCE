package strategies;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import problems.AbstractProblem;
import structures.Node;

public class DFSVisited extends DFS{
	
	protected List<String> visitados;
	
	public DFSVisited(AbstractProblem formulatedProblem) {
		super(formulatedProblem);
	}
	
	@Override
	public List<Node> expand(Node node) {
		List<Node> sucessors = new ArrayList<>();
		sucessorFn(node).forEach((estado, custo) ->{
			if (!visitados.contains(estado)) {				
				sucessors.add(nodeFactory.createNode(node, estado, custo));
				visitados.add(estado);
			}
		});
		return sucessors;
	}

	@Override
	public void initFringe() {
		fringe = new Stack<>();
		visitados  = new ArrayList<>();
		fringe.add(formulatedProblem.getInitialNode());
		visitados.add(formulatedProblem.getInitialNode().getEstado());
	}

}
