package structures;

public class Node {
	private Node pai;
	private String estado;
	private String acao;
	private int profundidade;
	private Integer custo;
	
	public Node() {
	}
	
	public Node(String estado) {
		this.estado = estado;
		this.pai = null;
		this.acao = null;
		this.custo = 0;
		this.profundidade = 0;
	}
	
	public Node(Node pai, String estado, String acao) {
		this.pai = pai;
		this.estado = estado;
		this.acao = acao;
		this.profundidade = (pai != null)? pai.getProfundidade() + 1 : 0;
	}

	public Node(Node pai, String estado, String acao, Integer custo) {
		this.pai = pai;
		this.estado = estado;
		this.acao = acao;
		this.custo = (pai!=null)? custo: custo;
		this.profundidade = (pai != null)? pai.getProfundidade() + 1 : 0;
	}
	
	public Integer getCusto() {
		return custo;
	}

	public void setCusto(Integer custo) {
		this.custo = custo;
	}

	public Node getPai() {
		return pai;
	}
	public void setPai(Node pai) {
		this.pai = pai;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}

	public int getProfundidade() {
		return profundidade;
	}

	public void setProfundidade(int profundidade) {
		this.profundidade = profundidade;
	}
	
	@Override
	public String toString() {
		return this.estado;
	}
	
}
