package structures;

import problems.AbstractProblem;

public class NodeFactory {

	AbstractProblem formulatedProblem;

	public NodeFactory(AbstractProblem formulatedProblem) {
		this.formulatedProblem = formulatedProblem;
	}

	public Node createNode(Node pai, String estado, Integer custo) {
		String acao = "";

		switch (formulatedProblem.getProblem()) {
		case ROMENIA:
			acao = "em(" + pai.getEstado() + ") -> ir(" + estado + ")";
			break;
		case ASPIRADOR:
			String estadoAnterior[] = pai.getEstado().split(",");
			int casaEsquerda = 0;
			int casaDireita = 1;
			int posicaoAspirador = 2;

			acao = "em(" + pai.getEstado() + ") -> ";
			if (((estadoAnterior[casaEsquerda].trim()).equals("sujo")
					&& (estadoAnterior[posicaoAspirador].trim()).equals("esquerda"))
					|| ((estadoAnterior[casaDireita].trim()).equals("sujo")
							&& (estadoAnterior[posicaoAspirador].trim()).equals("direita"))) {
				acao += "limpar";
			}

			if ((estadoAnterior[casaEsquerda].trim()).equals("limpo")
					&& (estadoAnterior[posicaoAspirador].trim()).equals("esquerda")) {
				acao += "mover para direita";
			} else if ((estadoAnterior[casaDireita].trim()).equals("limpo")
					&& (estadoAnterior[posicaoAspirador].trim()).equals("direita")) {
				acao += "mover para esquerda";
			}

			break;
		default:
			break;
		}
		
		return new Node(pai, estado, acao, custo);
	}

}
