package problems;

import java.util.HashMap;
import java.util.Map;

import structures.Node;
import util.FileReaderUtil;

public class VacuumCleanerProblem extends AbstractProblem {

	public VacuumCleanerProblem(String initialState, String objectiveState, Problem problem) {
		super(new Node(initialState), new Node(objectiveState), problem);
	}

	@Override
	public Map<String, Integer> getSucessors(String estado) {
		Map<String, Integer> sucessors = new HashMap<>();

		FileReaderUtil.readFile("aspirador.txt").forEach(line -> {
			String parts[] = line.split("->");
			if (parts[0].contains(estado)) {
				sucessors.put(parts[1].trim(), 0);
			}
		});
		
		return sucessors;
	}

	@Override
	public Float getHeuristic(String estado, String objetivo) {
		// TODO Auto-generated method stub
		return null;
	}

}
