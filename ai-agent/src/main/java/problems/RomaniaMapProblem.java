package problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import structures.Node;
import util.FileReaderUtil;

public class RomaniaMapProblem extends AbstractProblem {

	public RomaniaMapProblem(String initialState, String objectiveState, Problem problem) {
		super(new Node(initialState), new Node(objectiveState), problem);
	}

	@Override
	public Map<String, Integer> getSucessors(String estado) {
		Map<String, Integer> map = new HashMap<>();
		FileReaderUtil.readFile("romenia2.txt").forEach(line -> {
			String parts[] = line.split("->");
			List<String> neighbours = Arrays.asList(parts[1].trim().split(", "));
			if (parts[0].contains(estado))
				neighbours.forEach(n -> {
					map.put(n.split("\\(")[0], Integer.parseInt(n.substring(n.indexOf("(") + 1, n.indexOf(")"))));
				});
		});

		return map;
	}

	@Override
	public Float getHeuristic(String estado, String objetivo) {
		List<String> aux = new ArrayList<>();
		FileReaderUtil.readFile("heuristics/"+ objetivo + ".txt").forEach(line -> {
			String parts[] = line.split("->");
			if(parts[0].contains(estado))
				aux.add(line.substring(line.indexOf(": ") + 1, line.indexOf("miles")).trim());
		});
		return Float.parseFloat(aux.get(0));
	}

}
