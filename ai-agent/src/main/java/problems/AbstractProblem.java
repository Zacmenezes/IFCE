package problems;

import java.util.Map;

import structures.Node;

public abstract class AbstractProblem {
	
	protected Problem problem;
	private Node initialState;
	private Node objectiveState;
	
	public AbstractProblem(Node initialState, Node objectiveState, Problem problem) {
		this.problem = problem;
		this.initialState = initialState;
		this.objectiveState = objectiveState;
	}
	
	public boolean goalTest(Node node) {
		return node.getEstado().equals(objectiveState.getEstado());
	}

	public Node getInitialNode() {
		return initialState;
	}
	
	public String getInitialState() {
		return initialState.getEstado();
	}
	
	public String getObjectiveState() {
		return objectiveState.getEstado();
	}
	
	public Problem getProblem() {
		return problem;
	}
	
	public abstract Map<String, Integer> getSucessors(String estado);
	
	public abstract Float getHeuristic(String estado, String objetivo);
	
	@Override
	public String toString() {
		return problem.toString();
	}
}
