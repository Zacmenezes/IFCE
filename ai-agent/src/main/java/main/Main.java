package main;

import agents.SimpleAgent;
import problems.AbstractProblem;
import problems.Problem;
import problems.RomaniaMapProblem;
import problems.VacuumCleanerProblem;
import strategies.Strategy;
import util.OptionsUtil;

public class Main {

	public static void main(String[] args) {
		AbstractProblem formulatedProblem;
		SimpleAgent simpleAgent;
		String initialState;
		String objectiveState;
		int problemOption = 0;
		int strategyOption = 0;
		int limit = 0;
		Strategy strategy;
		OptionsUtil.createScanner();
		
		problemOption = OptionsUtil.getProblemOption();
		initialState = OptionsUtil.getInitialState();
		objectiveState = OptionsUtil.getObjectiveState();
		
		switch (problemOption) {
		case 0:
			formulatedProblem = new RomaniaMapProblem(initialState, objectiveState, Problem.ROMENIA);
			strategyOption = OptionsUtil.getRomaniaStrategyOption();
			break;
		case 1:
			formulatedProblem = new VacuumCleanerProblem(initialState, objectiveState, Problem.ASPIRADOR);
			strategyOption = OptionsUtil.getVaccumCleanerStrategyOption();
			break;
		default:
			formulatedProblem = null;
			break;
		}
		
		strategy = OptionsUtil.getStrategy(strategyOption);
		if(strategy == Strategy.DLS) {
			limit = OptionsUtil.getLimit();
		}
		
		simpleAgent = new SimpleAgent(strategy, formulatedProblem, limit);
		System.out.println("Ações:");
		simpleAgent.resolveProblem();
		System.out.println("\nProblema escolhido: " + formulatedProblem + "; Estratégia escolhida: " + strategy);
		
		OptionsUtil.closeScanner();
	}
	
}